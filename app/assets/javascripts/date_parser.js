DateParser = {
  // dateString = dd/mm/yyyy
  parse: function(dateString, delimeter) {
    var d = dateString.split(delimeter);
    return new Date(d[2], d[1]-1, d[0]);
  }
}
