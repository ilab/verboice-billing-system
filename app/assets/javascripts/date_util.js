DateUtil = {
  getFirstDayOfWeek: function(date) {
    var newDate = new Date(date);
    var day = newDate.getDay();
    // 0 to 6 = Sun to Sat
    var diff = newDate.getDate() - day + (day == 0 ? -0:0);
    return new Date(newDate.setDate(diff));
  },

  strftime: function(date) {
    var m = (date.getMonth() + 1);
    m = m < 10 ? ('0' + m) : m;
    var d = date.getDate() < 10 ? ('0' + date.getDate()) : date.getDate();
    return  d + "-" + m + "-" + date.getFullYear();
  }
}
