$(function(){
  $('.datepicker').datepicker({
    format: 'dd-mm-yyyy'
  })

  $(".btn-custom-range").on('click', function(){
    $("#customRange").modal({
      backdrop: 'static'
    });
  })

  $("#custom-range-ok").on('click', function(){
    $('#custome-range-error').hide();
    $(".btn-type").removeClass('active');

    start_date = $("#traffic_start_date").val();
    end_date = $("#traffic_end_date").val();

    if($.trim(start_date) == '' ||  $.trim(end_date) == '' ) {
      $('#custome-range-error').show();
    } else{
      $("#traffic_type").val('custom');
      setTraffic();
      submitData();
      $('#customRange').modal('hide');
    }
  })

  $(".btn-type").on('click', function(){
    $(".btn-type").removeClass('active');

    $this = $(this);
    $this.addClass('active');
    $("#traffic_type").val($this.data().type);

    // set to current date
    var date = new Date();
    $("#traffic_start_date").val(DateUtil.strftime(date));
    setTraffic();

    moveBy(0);

    return false;
  })

  $("#traffic_today").on('click', function(){
    window.location.href = window.location.pathname;
    return false;
  })

  $("#date-previous").on('click', function() {
     moveBy(-1);
     return false;
  });

  $("#date-next").on('click', function() {
    moveBy(1);
    return false;
  });

  $("#hide_no_traffic").on('click', function() {
    setTraffic();
    submitData();
  });
});

function moveBy(step) {
  var startDate = DateParser.parse($("#traffic_start_date").val(), "-");
  var type = $.trim($("#traffic_type").val());

  if(type  == 'day'){
    var offset = step;
    start = new Date(startDate.getFullYear(), startDate.getMonth(), startDate.getDate() + offset )
    end = new Date(start.getFullYear(), start.getMonth(), start.getDate()) 
  }

  else if(type == 'week') {
    var offset = step * 7;
    start = DateUtil.getFirstDayOfWeek(new Date(startDate.getFullYear(), startDate.getMonth(), startDate.getDate() + offset));
    end = new Date(start.getFullYear(), start.getMonth(), start.getDate() + Math.abs(offset == 0 ? 7 : offset) - 1)
  }

  else if (type == 'month'){
    var offset = step;
    start = new Date(startDate.getFullYear(), startDate.getMonth() + offset, 1)
    end = new Date(start.getFullYear(), start.getMonth() + Math.abs(offset == 0 ? 1 : offset), 0)
  }

  // set form data
  setDate(start, end);
  setTraffic();

  submitData();
}

function submitData(){
  $("#form-traffic").submit();
}

function setDate(startDate, endDate){
  var startDateString = DateUtil.strftime(startDate);
  var endDateString = DateUtil.strftime(endDate);
  $("#traffic_start_date").val(startDateString);
  $("#traffic_end_date").val(endDateString);
  $("#date_text").html(startDateString + ", " + endDateString);
}

function setTraffic() {
  $("#has_traffic").val($('#hide_no_traffic').prop('checked'));
}
