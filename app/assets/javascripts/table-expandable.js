$(function(){
  tableExpandable();
})

function tableExpandable(){
  $(".td-expandable").on("click", function(){
    $this = $(this)
    var title = $(this).attr('title')
    var content = $(this).html();

    $this.attr('title', content);
    $this.html(title);

  })
}