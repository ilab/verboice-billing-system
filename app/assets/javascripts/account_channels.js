function accountChanged(element){
  element.on('change',function(){
    var elementId = element.attr("id");

    if(element.val()) {
      var email = $("#" +elementId +" option:selected").text();
      $("#account_email_field").val(email);

      $.ajax({
        method: 'GET',
        url: config['host'] + "channels.json",
        data: {account_id: element.val()},
        success: function(channels){
          addChannelsTo(channels, getChildId(elementId));
        }
      });
    } else {
      addChannelsTo([], getChildId(elementId));
    }
  });
};

function channelChanged(element) {
  element.on('change',function(){
    if(element.val()) {
      var channelName = $("#" +element.attr("id") +" option:selected").text();
      $("#channel_name_field").val(channelName);
    }
  });
};

function addChannelsTo(channels, elementId){
  var defaultOption = defaultChannelOptionText(elementId);
  channels.unshift(defaultOption);

  options = $.map(channels, function(item){
    return "<option value='" + item.id + "' >" + item.name + "</options>";
  });

  $("#" +elementId).html(options);
};

function defaultChannelOptionText(elementId){
  var defaultOptionText = elementId == "channel_id" ? '--- All channel ---' :  '--- Select channel ---';
  var defaultOption = {id: '', name: defaultOptionText};
  return defaultOption;
}

function getChildId(parentId){
  return parentId == "account_id" ? "channel_id" : "channel_id_field";
};