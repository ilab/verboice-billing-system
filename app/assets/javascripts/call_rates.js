$(function(){
  var $accountId = $("#account_id");
  accountChanged($accountId);

  var $accountIdField = $("#account_id_field");
  accountChanged($accountIdField);
  
  var $channelIdField = $("#channel_id_field");
  channelChanged($channelIdField);
})
