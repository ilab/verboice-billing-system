class Call
  attr_reader :call_log, :call_rate

  def initialize call_log, call_rate = nil
    @call_log = call_log
    @call_rate = call_rate
  end

  def rate
    return 0 if !has_call_rate?
    call_log.incoming? ? call_rate.incoming_rate : call_rate.outgoing_rate
  end

  def round_rate
    has_call_rate? ? call_rate.round_rate : 0
  end

  def cost
    has_error? ? 0 : call_rate.cost_for(call_log)
  end

  # error when it missed call rate
  def has_error?
    !has_call_rate?
  end

  def has_call_rate?
    !call_rate.nil?
  end
end
