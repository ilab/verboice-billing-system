class QuotaSync
  def self.with_credential instance, options={}

    email     = ENV['EMAIL']
    secret    = ENV['SECRETE']
    endpoint  =  instance.end_point

    current_access = Session.login(endpoint, email , secret)

    if(Session.success?)
      current_access[:endpoint] = endpoint
      ActiveApi.init_auth(current_access)
      options[:success].call(instance, options[:data])
    else
      options[:error].call(instance)
    end
  end

end