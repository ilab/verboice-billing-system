class Service::Channel < ActiveApi
  attribute :id, Integer
  attribute :name, String
  attribute :number, String
  attribute :status, String
  attribute :kind, String
  attribute :account, Service::Account
  attribute :call_logs, Array[Service::CallLog]
  attribute :created_at, String

  STATUS_PENDING = 'pending'
  STATUS_PENDING_DISPLAY = 'Inactive'
  STATUS_PENDING_ACTION = 'Deactivate'

  STATUS_APPROVED = 'approved'
  STATUS_APPROVED_DISPLAY = 'Active'
  STATUS_APPROVED_ACTION = 'Activate'

  STATUSES = [
    [STATUS_PENDING_DISPLAY, STATUS_PENDING],
    [STATUS_APPROVED_DISPLAY, STATUS_APPROVED]
  ]

  CHANNEL_VISIBLED = 'sip'

  @@channels = {}

  def self.collection account_id
    account_id ? fetch(account_id).map{|channel| [channel.name, channel.id] } : []
    # fetch(account_id).map{|channel| [channel.name, channel.id] }
  end

  def self.fetch account_id
    @@channels[account_id] || self.get_channels(account_id)
  end

  def self.get_channels account_id
    @@channels[account_id] = account_id ? where(account_id: account_id) : all
  end

  def self.deactivate id
    update(id, 'deactivate')
  end

  def self.activate id
    update(id, 'activate')
  end

end
