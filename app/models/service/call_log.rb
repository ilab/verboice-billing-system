class Service::CallLog < ActiveApi
  attribute :prefix_called_number, String 
  attribute :address, String
  attribute :duration, Integer
  attribute :direction, String
  attribute :channel, Service::Channel
  attribute :called_at, DateTime

  def incoming?
    self.direction == 'incoming'
  end

  def outgoing?
    self.direction == 'outgoing'
  end

  def self.filter channel_ids, start_date, end_date, direction = nil, address = nil
    conditions = {
      channel_id: channel_ids,
      start_date: start_date,
      end_date: end_date,
    }

    conditions[:address] = address if address
    conditions[:direction] = direction if direction

    Service::CallLog.where(conditions)
  end

end
