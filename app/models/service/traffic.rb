class Service::Traffic < ActiveApi
  attribute :id, Integer
  attribute :name, String 
  attribute :prefix_called_number, String
  attribute :account, Service::Account 
  attribute :traffics, Array[Service::Consumtion]
end
