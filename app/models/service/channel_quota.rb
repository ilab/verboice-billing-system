class Service::ChannelQuota < ActiveApi
  attribute :id, Integer
  attribute :channel_id, Integer
  attribute :enabled, Boolean
  attribute :blocked, Boolean
  attribute :total, Float
  attribute :used, Float

  def self.save_quota quota, options= {}
    params = { channel_id:   quota.channel_id,
               enabled:      quota.enabled,
               blocked:      quota.blocked,
               total:        quota.total,
               used:         quota.used
             }

    channel_quota = Service::ChannelQuota.create(params)
    quota.channel_quota_id = channel_quota.id
    quota.save
  end

  def self.update_quota quota, options={}
    enabled_changed = (quota.enabled != options[:enabled])
    status_changed  = (quota.blocked != options[:blocked])
    self.save_quota(quota) if(enabled_changed || status_changed)
  end
end
