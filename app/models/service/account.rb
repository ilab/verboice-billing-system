class Service::Account < ActiveApi
  attribute :id, Integer
  attribute :email, String

  def self.collection
    all.map{|account| [account.email, account.id]}
  end

end
