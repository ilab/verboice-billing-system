module ByInstance
  def by_instance instance
    where(["instance_id = ?", instance.id])
  end

  def by_account account_id
    where(account_id: account_id)
  end

  def by_channel channel_id
    where(channel_id: channel_id)
  end
end