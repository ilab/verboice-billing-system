class CallRate < ActiveRecord::Base
  validates :account_id, :channel_id, :incoming_rate, :outgoing_rate, :prefix, presence: true
  validates :incoming_rate, :outgoing_rate, :round_rate, numericality:  { greater_than_or_equal_to: 0}
  validates :channel_id, uniqueness: {scope: [:prefix, :instance_id] }

  validate :prefix_no_white_space
  validate :unique_prefix
  attr_accessor :address

  DEFAULT_ROUND = 0

  belongs_to :instance
  extend ByInstance
  
  def unique_prefix
    call_rates = CallRate.where(['channel_id=:channel_id', channel_id: self.channel_id])

    call_rates.each do |call_rate|
      if call_rate != self && call_rate.has_prefix?(self.prefix)
        errors.add :prefix, "already exists for channel #{self.channel_name}"
      end
    end
  end

  def prefix_no_white_space
    if self.prefix.present? && self.prefix.include?(" ")
      errors.add :prefix, "white space is not allowed. please clean it up"
    end
  end

  def has_prefix? p
    prefixes = p.split(',')
    current_prefixes = self.prefix.split(",")

    prefixes.each do |temp_pref|
      return true if current_prefixes.include?(temp_pref)
    end
    return false
  end

  def has_address? address
    prefixes = self.prefix.split(",")
    prefixes.each do |p|
      return true if address.start_with?(p)
    end
    false
  end

  def cost_for call_log
    rate_cost = rate_for call_log

    if(self.round_rate == CallRate::DEFAULT_ROUND)
      call_rate_per_second = (rate_cost / 60).to_f
      cost = (call_log.duration.nil? ? 0 : call_log.duration) * call_rate_per_second
    else
      call_rate_per_round = (self.round_rate * rate_cost) / 60
      cost = (call_log.duration.to_f / self.round_rate).ceil * call_rate_per_round
    end
    
    cost
  end 

  def rate_for call_log
    call_log.incoming? ? self.incoming_rate : self.outgoing_rate
  end     

end
