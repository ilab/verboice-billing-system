class QuotaHistory < ActiveRecord::Base
  belongs_to :quota
  belongs_to  :instance

  extend ByInstance

  def enabled_as_text
    self.enabled ? 'Yes' : 'No'
  end

  def block_if_reached_as_text
    self.block_if_reached ? 'Block' : 'Allow'
  end

  def self.as_enabled
    where(['enabled = ?', true])
  end

  def blocked_as_text
    self.blocked ?  'Blocked' : 'OK'
  end

  def current_percentage
    self.used * 100 / self.total
  end

  def self.create_from quota
    due_date = self.due_date_from_quota quota

    history = quota.quota_histories.find_by due_date: due_date
    history = quota.quota_histories.build if !history

    history.due_date         = due_date
    history.total            = quota.total
    history.used             = quota.used
    history.from_date        = quota.from_date
    history.to_date          = quota.to_date
    history.channel_id       = quota.channel_id
    history.channel_name     = quota.channel_name
    history.account_id       = quota.account_id
    history.account_email    = quota.account_email
    history.enabled          = quota.enabled
    history.alert_email      = quota.alert_email
    history.email_sent       = quota.email_sent
    history.block_if_reached = quota.block_if_reached
    history.blocked          = quota.blocked
    history.instance         = quota.instance

    history.percentage_condition  = quota.percentage_condition
    history.save
    history
  end

  def self.due_date_from_quota quota
    quota.from_date ? (quota.from_date.beginning_of_month.to_date + 1.month) : Time.zone.now.beginning_of_month.to_date
  end

end
