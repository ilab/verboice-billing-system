class Quota < ActiveRecord::Base
  self.table_name = 'quotas'
  validates :account_id, :channel_id, :total, presence: true
  validates :total, numericality:  { greater_than: 0}
  validates :channel_id, uniqueness: {scope: :instance_id}

  before_save :set_call_blocked
  belongs_to  :instance

  has_many :quota_histories

  extend ByInstance

  def enabled_as_text
    self.enabled ? 'Yes' : 'No'
  end

  def block_if_reached_as_text
    self.block_if_reached ? 'Block' : 'Allow'
  end

  def self.as_enabled
    where(['enabled = ?', true])
  end

  def current_percentage
    self.used * 100 / self.total
  end

  def warning?
    # remaining quota less than percentate_condition
    (100 - self.current_percentage) < self.percentage_condition
  end

  def blocked_as_text
    self.blocked ?  'Blocked' : 'OK'
  end

  def is_reached?
    self.used >= self.total
  end

  private
  def set_call_blocked
    if self.is_reached?
      self.blocked = 1 if self.block_if_reached
      self.blocked = 0 if !self.block_if_reached
    else
      self.blocked = 0
    end
  end

end