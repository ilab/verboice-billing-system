module Traffic
  class Address < Record

    attribute :address, String, default: nil
    attribute :incoming_amount, Float, default: 0
    attribute :outgoing_amount, Float, default: 0
    attribute :missing_rate, Boolean, default: false

    def increase_call!(call)
      if call.call_log.incoming?
        self.incoming_call_duration += (call.call_log.duration.to_f / 60) if call.call_log.duration
        self.incoming_call_count += 1
        self.incoming_amount += call.cost
      else
        self.outgoing_call_duration += (call.call_log.duration.to_f / 60) if call.call_log.duration
        self.outgoing_call_count += 1
        self.outgoing_amount += call.cost
      end
    end

    def has_error?
      self.missing_rate
    end

  end
end
