class Traffic::Record
  include Virtus.model
  
  attribute :channel_id, Integer
  attribute :channel_name, String

  attribute :account_id, Integer
  attribute :account_email, String

  attribute :incoming_rate, Float, default: 0
  attribute :outgoing_rate, Float, default: 0

  attribute :incoming_call_duration, Float, default: 0
  attribute :outgoing_call_duration, Float, default: 0

  attribute :incoming_call_count, Integer, default: 0
  attribute :outgoing_call_count, Integer, default: 0

  def has_traffic?
    has_incoming? || has_outgoing?
  end

  def has_incoming?
    incoming_call_count > 0
  end

  def has_outgoing?
    outgoing_call_count > 0
  end

end