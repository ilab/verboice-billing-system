class LoginForm
  include ActiveModel::Validations
  include ActiveModel::Conversion
  extend ActiveModel::Naming

  attr_accessor :endpoint, :email, :password

  validates :endpoint, :email, :password, presence: true

  def initialize params={}
    @endpoint = params[:endpoint]
    @email    = params[:email]
    @password = params[:password]
  end
end
