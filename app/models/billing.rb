class Billing
  attr_reader :calls

  def initialize channel_id
    @channel_id = channel_id

    @calls  = []
  end

  def for start_date, end_date, direction = nil, address = nil
    process!(transactions(start_date, end_date, direction, address))
  end

  def process! call_logs
    @calls = Billable.new(call_rates: call_rates).for(call_logs)
  end

  def total
    @calls.inject(0) { |sum, call| sum + call.cost }
  end

  def error_calls!
    @calls = @calls.select { |call| call.has_error? }
  end

  def has_error?
    @calls.any? { |call| call.has_error? }
  end

  def group_by_address
    addresses = {}

    @calls.each do |call|
      address = addresses.key?(call.call_log.address) ? addresses[call.call_log.address] : Traffic::Address.new(address: call.call_log.address, channel_id: @channel_id)
      address.increase_call!(call)
      address.missing_rate = call.has_error? unless address.missing_rate
      addresses[call.call_log.address] = address
    end

    addresses
  end

  def transactions start_date, end_date, direction = nil, address = nil
    Service::CallLog.filter(@channel_id, start_date, end_date, direction, address)
  end

  def call_rates
    CallRate.where(['channel_id = :channel_id', channel_id: @channel_id])
  end
 end
