class Billable
  include Virtus.model

  attribute :call_rates, Array, default: []

  def for call_logs
    calls = []

    call_logs.each do |call_log|
      call = Call.new call_log
      call_rates.each do |call_rate|
        if call_rate.has_address?(call_log.address)
          call = Call.new(call_log, call_rate)
          break
        end
      end

      calls << call
    end

    calls
  end
end
