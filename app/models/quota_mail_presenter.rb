class QuotaMailPresenter
  def self.by quota
    if quota.warning? && !quota.email_sent && quota.alert_email.present?
      QuotaMailer.warning_email(quota).deliver
      quota.email_sent = Time.zone.now
    end
    quota
  end

end