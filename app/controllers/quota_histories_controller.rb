class QuotaHistoriesController < ApplicationController
  def index
    @due_date = due_date
    @quota_histories = params[:quota_id].present? ? Quota.find(params[:quota_id]).quota_histories : QuotaHistory.all
    @quota_histories = @quota_histories.by_instance(instance).where(['due_date = ?', @due_date]).page(params[:page])
  end

  private
  def due_date
    if params[:due_date].present?
      Date.strptime(params[:due_date], '%Y-%m-%d')
    else
      Time.zone.now.beginning_of_month.to_date
    end
  end
end