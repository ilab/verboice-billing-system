class QuotasController < ApplicationController
  def index
    @quotas = Quota.by_instance(instance).page(params[:page])
  end

  def new
    @quota = Quota.new
  end

  def create
    @quota = Quota.new filter_params

    @quota.from_date = Time.zone.now.beginning_of_month
    @quota.to_date  = Time.zone.now.end_of_month

    if @quota.save
      Service::ChannelQuota.save_quota(@quota)

      flash[:success] = "Quota has been created"
      redirect_to quotas_path()
    else
      flash.now[:error] = "Failed to create quota"
      render :new
    end
  end

  def edit
    @quota = Quota.find params[:id]
  end

  def update
    @quota = Quota.find params[:id]

    options = {
      enabled: @quota.enabled,
      blocked: @quota.blocked
    }

    if @quota.update_attributes filter_params
      Service::ChannelQuota.update_quota(@quota, options)
      flash[:success] = "Quota has been updated successfully"
      redirect_to quotas_path()
    else
      flash.now[:error] = "Failed to update quota"
      render :edit
    end
  end

  def destroy
    begin
      @quota = Quota.find params[:id]
      channel_quota_id = @quota.channel_quota_id
      @quota.destroy
      Service::ChannelQuota.destroy(channel_quota_id)
      flash[:success]  = "Quota has been removed successfully"
    rescue Exception => e
      flash.now[:error] = e.message
    end
    redirect_to quotas_path
  end

  private

  def filter_params
    attributes = params.require(:quota).permit(:account_id, :account_email, :channel_name, :channel_id, :total,
     :used, :alert_email, :block_if_reached, :enabled, :percentage_condition)
    with_instance(attributes)
  end
end