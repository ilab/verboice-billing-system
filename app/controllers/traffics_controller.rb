class TrafficsController < ApplicationController

  def index
    if(!params[:start_date].present?)
      start_time = Time.now
      end_time = start_time
    else
      start_time = Time.parse(params[:start_date])
      end_time = Time.parse(params[:end_date])
    end

    params[:start_date] = start_time.beginning_of_day.to_date.to_string(Date::DEFAULT_FORMAT)
    params[:end_date]   = end_time.end_of_day.to_date.to_string(Date::DEFAULT_FORMAT)
    params[:type]       = 'day' if !params[:type].present?

    conditions = {
      start_date: start_time.beginning_of_day.to_string(Time::DB_FORMAT),
      end_date: end_time.end_of_day.to_string(Time::DB_FORMAT)
    }

    channels = Service::Traffic.all(conditions)

    ids = channels.map{|channel| channel.id}.join(',')

    rates = CallRate.by_instance(instance).where(" channel_id in ( #{ids} ) ")
    @headers = {
      incoming_call: 0,
      outgoing_call: 0,
      incoming_call_duration: 0,
      outgoing_call_duration: 0
    }
    @records = []

    channels.each do |channel|
      record = Traffic::Record.new

      record.channel_id    =  channel.id
      record.channel_name  = channel.name

      record.account_id    = channel.account.id
      record.account_email = channel.account.email

      rate = rates.select do |rate| 
        rate.channel_id == channel.id  && 
        rate.account_id == channel.account.id
      end.first  

      if(rate)
        record.incoming_rate = rate.incoming_rate
        record.outgoing_rate = rate.outgoing_rate
      end

      record.incoming_call_duration = 0
      record.outgoing_call_duration = 0

      record.incoming_call_count = 0
      record.outgoing_call_count = 0

      channel.traffics.each do |traffic|
        total_duration = traffic.total_duration.to_f/60

        if traffic.direction == 'incoming'
          record.incoming_call_duration += total_duration
          record.incoming_call_count += traffic.total_call
        else
          record.outgoing_call_duration += total_duration
          record.outgoing_call_count += traffic.total_call
        end
      end

      @headers[:incoming_call_duration] += record.incoming_call_duration
      @headers[:incoming_call] += record.incoming_call_count
      @headers[:outgoing_call_duration] += record.outgoing_call_duration
      @headers[:outgoing_call] += record.outgoing_call_count

      @traffic = params[:traffic].present? && params[:traffic].to_boolean == true

      if @traffic
        @records << record if record.has_traffic?
      else
        @records << record
      end
    end
  end
end
