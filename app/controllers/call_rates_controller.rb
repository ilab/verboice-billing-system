class CallRatesController < ApplicationController
  def index
    @call_rates = CallRate.by_instance(instance)
    @call_rates = @call_rates.by_account(params[:account_id]) if params[:account_id].present?
    @call_rates = @call_rates.by_channel(params[:channel_id]) if params[:channel_id].present?

    @call_rates = @call_rates.page(params[:page])
  end
  
  def new
    @call_rate = CallRate.new
  end

  def create
    @call_rate = CallRate.new filter_params

    if(@call_rate.save)
      flash[:success] =  'Call rate has been created'
      redirect_to call_rates_path
    else
      flash.now[:error] = 'Failed to create call rate'
      render :new
    end
  end

  def edit
    @call_rate = CallRate.find params[:id]
  end

  def update
    @call_rate = CallRate.find params[:id]
    if(@call_rate.update_attributes(filter_params))
      flash[:success] = "Call rate has been udated"
      redirect_to call_rates_path()
    else
      flash.now[:error] = "Failed to update call rate"
      render :edit  
    end
  end

  def destroy
    begin
      @call_rate = CallRate.find params[:id]
      @call_rate.destroy
      flash[:success] = 'Call rate has been deleleted'
    rescue Exception => e
      flash[:error] = e.message
    end

    redirect_to call_rates_path
  end

  def filter_params
    attributes = params.require(:call_rate).permit(:account_id, :account_email, :channel_name, :channel_id, :prefix,
     :incoming_rate, :outgoing_rate, :note, :round_rate)
    with_instance(attributes)
  end


end
