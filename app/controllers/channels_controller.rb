class ChannelsController < ApplicationController
  def index
    conditions = {}
    conditions[:account_id] = params[:account_id] if(params[:account_id])
    conditions[:status] =  params[:status] if params[:status].present?

    service_channels = Service::Channel.all(conditions)

    @channels = service_channels

    if params[:number].present?
      @channels = service_channels.select { |channel| channel if channel.number && channel.number.match(params[:number]) }
    end
    
    respond_to do |format|
      format.html
      format.json { render json: @channels}
    end
  end

  def deactivate
    Service::Channel.deactivate(params[:id])
    redirect_to channels_path
  end

  def activate
    Service::Channel.activate(params[:id])
    redirect_to channels_path
  end

end
