class BillingsController < ApplicationController
  before_action :initialize_billing

  helper_method :round_rate

  # GET /billings
  def index
    @billing.error_calls! if error?
    @addresses = @billing.group_by_address if grouped? || error?

    if error?
      render "billings/error"
      return
    end
  end

  private

  def initialize_billing
    start_time = Time.parse(params[:start_date])
    end_time = Time.parse(params[:end_date])

    @billing = Billing.new(params[:channel_id])

    @billing.for(start_time.beginning_of_day.to_string(Time::DB_FORMAT),
      end_time.end_of_day.to_string(Time::DB_FORMAT),
      params[:direction], params[:address])
  end

  def grouped?
    @grouped = params[:group] === true || params[:group] === 'true' || params[:group] === 1 || params[:group] === '1'
  end

  def error?
    @error = params[:error].nil? ? false : true
  end

end
