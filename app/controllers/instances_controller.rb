class InstancesController < ApplicationController
  # GET /instances
  def index
    @instances = Instance.all.page(params[:page])
  end

  # GEt /instances/new
  def new
    @instance = Instance.new
  end

  # POST /instances
  def create
    @instance = Instance.new instance_params

    if(@instance.save)
      flash[:success] =  'Instance has been created'
      redirect_to instances_path
    else
      flash.now[:error] = 'Failed to create instance'
      render :new
    end
  end

  def edit
    @instance = Instance.find(params[:id])
  end

  def update
    @instance = Instance.find(params[:id])
    
    if @instance.update_attributes(instance_params)
      flash[:success] =  'Instance has been updated'
      redirect_to instances_path
    else
      flash.now[:error] = 'Failed to update instance'
      render :edit
    end
  end

  def destroy
    @instance = Instance.find(params[:id])
    
    if Instance.count > Instance::MINIMUM
      if @instance.destroy
        flash[:success] =  "Instance '#{@instance.name}' has been deleted"
        redirect_to instances_path
      else
        flash[:error] = 'Failed to delete instance'
        redirect_to instances_path
      end
    else
      flash[:error] = 'Instance must be at least one'
      redirect_to instances_path
    end
  end

  private

  def instance_params
    params.require(:instance).permit(:name, :url, :end_point)
  end

end