class QuotaMailer < ActionMailer::Base
  default from: ENV['FROM_EMAIL']

  def warning_email(quota)
    @quota = quota
    email = @quota.alert_email.split(",").map{|m| m.strip}

    subject = "Quota reached #{@quota.percentage_condition}% on #{@quota.instance.name}"
    mail(to: email, subject: subject)
  end
end
