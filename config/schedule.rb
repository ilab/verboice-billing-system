set :output,"log/cron.log"

every 1.minutes do
  rake "quota:update"
end

every "0 0 1 * *" do
  rake "quota:reset"
end
