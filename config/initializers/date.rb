class Date
  
  DEFAULT_FORMAT  = '%d-%m-%Y'

  def to_string format = Date::DEFAULT_FORMAT
    self.strftime(format)
  end

end