class String
  def to_boolean
    self == 'true'
  end
end