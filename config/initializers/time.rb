class Time
  
  DB_FORMAT = "%Y-%m-%d %H:%M:%S %z"
  DISPLAY_FORMAT = '%d-%m-%Y %H:%M:%S %z'

  def to_string format = Time::DISPLAY_FORMAT, time_zone = "UTC"
    zone = self.in_time_zone(time_zone)
    zone.strftime(format)
  end

end