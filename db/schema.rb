# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20140901094254) do

  create_table "call_rates", force: true do |t|
    t.integer  "account_id"
    t.string   "account_email"
    t.string   "channel_name"
    t.string   "prefix"
    t.float    "incoming_rate"
    t.float    "outgoing_rate"
    t.text     "note"
    t.integer  "channel_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "round_rate",    default: 0
    t.integer  "instance_id"
  end

  create_table "errors", force: true do |t|
    t.string   "usable_type"
    t.integer  "usable_id"
    t.text     "class_name"
    t.text     "message"
    t.text     "trace"
    t.text     "target_url"
    t.text     "referer_url"
    t.text     "params"
    t.text     "user_agent"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "instances", force: true do |t|
    t.string   "name"
    t.string   "url"
    t.string   "end_point"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "quota_histories", force: true do |t|
    t.float    "total",                default: 0.0
    t.float    "used",                 default: 0.0
    t.date     "from_date"
    t.date     "to_date"
    t.integer  "channel_id"
    t.string   "channel_name"
    t.integer  "account_id"
    t.string   "account_email"
    t.boolean  "enabled",              default: true
    t.string   "alert_email"
    t.datetime "email_sent"
    t.float    "percentage_condition", default: 0.0
    t.boolean  "block_if_reached",     default: true
    t.boolean  "blocked",              default: false
    t.date     "due_date"
    t.integer  "quota_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "instance_id"
  end

  create_table "quotas", force: true do |t|
    t.float    "total",                default: 0.0
    t.float    "used",                 default: 0.0
    t.date     "from_date"
    t.date     "to_date"
    t.integer  "channel_id"
    t.string   "channel_name"
    t.integer  "account_id"
    t.string   "account_email"
    t.boolean  "enabled",              default: true
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "alert_email"
    t.datetime "email_sent"
    t.float    "percentage_condition", default: 0.0
    t.boolean  "block_if_reached",     default: true
    t.boolean  "blocked",              default: false
    t.integer  "instance_id"
    t.integer  "channel_quota_id"
  end

end
