class AddInstanceIdToQuotaHistoriesTable < ActiveRecord::Migration
  def change
    add_column :quota_histories, :instance_id, :integer
  end
end
