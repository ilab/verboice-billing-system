class CreateQuotas < ActiveRecord::Migration
  def change
    create_table :quotas do |t|

      t.float :total, default: 0
      t.float :used, default: 0

      t.date :from_date
      t.date :to_date

      t.integer :channel_id
      t.string :channel_name

      t.integer :account_id
      t.string :account_email

      t.boolean :enabled, default: true

      t.timestamps
    end
  end
end
