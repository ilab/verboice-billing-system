class AddReachedColumnToQuotasTable < ActiveRecord::Migration
  def change
    add_column :quotas, :blocked, :boolean, default: false
  end
end
