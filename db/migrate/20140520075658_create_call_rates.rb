class CreateCallRates < ActiveRecord::Migration
  def change
    create_table :call_rates do |t|
      t.integer :account_id
      t.string :account_email
      t.string :channel_name
      t.string :prefix
      t.float :incoming_rate
      t.float :outgoing_rate
      t.text :note
      t.integer :channel_id

      t.timestamps
    end
  end
end
