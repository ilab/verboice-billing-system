class AddAlertEmailPercentageConditionBlockageToQuotasTable < ActiveRecord::Migration
  def change
    add_column :quotas, :alert_email, :string
    add_column :quotas, :email_sent, :datetime
    add_column :quotas, :percentage_condition, :float, default: 0
    add_column :quotas, :block_if_reached, :boolean, default: true
  end
end
