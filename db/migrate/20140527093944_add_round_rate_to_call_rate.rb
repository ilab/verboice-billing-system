class AddRoundRateToCallRate < ActiveRecord::Migration
  def change
    add_column :call_rates, :round_rate, :integer, default: CallRate::DEFAULT_ROUND
  end
end
