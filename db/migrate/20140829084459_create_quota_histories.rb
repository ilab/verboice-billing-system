class CreateQuotaHistories < ActiveRecord::Migration
  def change
    create_table :quota_histories do |t|
      t.float    "total",                default: 0.0
      t.float    "used",                 default: 0.0
      t.date     "from_date"
      t.date     "to_date"

      t.integer  "channel_id"
      t.string   "channel_name"

      t.integer  "account_id"
      t.string   "account_email"

      t.boolean  "enabled",              default: true

      t.string   "alert_email"
      t.datetime "email_sent"

      t.float    "percentage_condition", default: 0.0
      t.boolean  "block_if_reached",     default: true
      t.boolean  "blocked",              default: false
      t.date     "due_date"

      t.references :quota
      t.timestamps


    end
  end
end
