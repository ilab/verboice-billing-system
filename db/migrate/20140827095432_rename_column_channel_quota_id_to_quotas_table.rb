class RenameColumnChannelQuotaIdToQuotasTable < ActiveRecord::Migration
  def change
    add_column :quotas, :channel_quota_id, :integer
  end
end
