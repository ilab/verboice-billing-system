require 'spec_helper'

describe Service::CallLog do

  describe '.incoming?' do
    it 'return true if call has direction eq to incoming' do
      call = Service::CallLog.new direction: 'incoming'
      expect(call.incoming?).to eq(true)
    end

    it 'return false if call\'s direction is not incoming ' do
      call = Service::CallLog.new direction: 'outgoing'
      expect(call.incoming?).to eq(false)
    end
  end

end
