require 'spec_helper'

describe Instance do
  it { should validate_presence_of(:name) }
  it { should validate_presence_of(:url) }
  it { should validate_presence_of(:end_point) }

  it { should validate_uniqueness_of(:name) }
  it { should validate_uniqueness_of(:url) }
  it { should validate_uniqueness_of(:end_point) }

  it { should allow_value('http://foo.com', 'http://bar.com/baz').for(:url) }
  it { should_not allow_value('asdfjkl').for(:url) }

  it { should allow_value('http://foo.com', 'http://bar.com/baz').for(:end_point) }
  it { should_not allow_value('asdfjkl').for(:end_point) }
end
