require 'spec_helper'

describe CallRate do
  it { should validate_presence_of(:account_id) }

  it { should validate_presence_of(:channel_id) }

  it { should validate_presence_of(:prefix) }

  it { validate_uniqueness_of(:channel_id).scoped_to(:prefix) }

  it { should validate_presence_of(:incoming_rate) }
  it { should validate_numericality_of(:incoming_rate).is_greater_than_or_equal_to(0) }

  it { should validate_presence_of(:outgoing_rate) }
  it { should validate_numericality_of(:outgoing_rate).is_greater_than_or_equal_to(0) }

  describe 'validate unique prefix' do
    context "unique prefix" do
      it 'allow to save' do
        attrs = {
          channel_id: 3,
          channel_name: 'Smart',
          prefix: '010,012,16',
          account_id: 1,
          incoming_rate: 10,
          outgoing_rate: 20
        }

        create(:call_rate, channel_id: 2, channel_name: 'Smart', prefix: '012,015,017')
        call_rate = CallRate.new attrs
        result = call_rate.save
        expect(result).to be true
        expect(call_rate.errors.full_messages.size).to eq 0
      end
    end

    context 'duplicate prefix' do
      it 'does not allow to save for new record' do
        attrs = {
          channel_id: 2,
          channel_name: 'Smart',
          prefix: '010,012,16',
          account_id: 1,
          incoming_rate: 10,
          outgoing_rate: 20
        }

        create(:call_rate, channel_id: 2, channel_name: 'Smart', prefix: '012,015,017')
        call_rate = CallRate.new attrs
        result = call_rate.save
        expect(result).to be false
        expect(call_rate.errors.full_messages[0]).to eq 'Prefix already exists for channel Smart'
      end

      it 'allow to update' do
        call_rate = create(:call_rate, channel_id: 2, channel_name: 'Smart', prefix: '012,015,017')
        call_rate.prefix = '010,015,017'
        result = call_rate.save
        expect(result).to eq true
        expect(call_rate.prefix).to eq '010,015,017'
        expect(call_rate.errors.full_messages.size).to eq 0
      end
    end


  end

  describe '.cost_for' do
    before(:each) do
      @call_log1  = Service::CallLog.new(duration: 30, direction: 'incoming')
      @call_log2  = Service::CallLog.new(duration: 60, direction: 'incoming')
      @call_log3  = Service::CallLog.new(duration: 70, direction: 'outgoing')
      @call_log4  = Service::CallLog.new(duration: 10, direction: 'outgoing')

    end
    context 'round_rate set' do
      it 'calculate cost for call_log by each round_rate' do
        call_rate = create(:call_rate, incoming_rate: 6 , outgoing_rate: 12, round_rate: 20 )

        expect(call_rate.cost_for(@call_log1)).to eq(20*2*6/60)
        expect(call_rate.cost_for(@call_log2)).to eq(20*3*6/60)
        expect(call_rate.cost_for(@call_log3)).to eq(20*4*12/60)
        expect(call_rate.cost_for(@call_log4)).to eq(20*12/60)
      end
    end
    
    context 'round_rate not set'  do
      it 'calculate cost for normal time' do
        call_rate = create(:call_rate, incoming_rate: 6 , outgoing_rate: 12, round_rate: CallRate::DEFAULT_ROUND )

        expect(call_rate.cost_for(@call_log1)).to eq(30*6/60)
        expect(call_rate.cost_for(@call_log2)).to eq(60*6/60)
        expect(call_rate.cost_for(@call_log3)).to eq(70*12/60)
        expect(call_rate.cost_for(@call_log4)).to eq(10*12/60)
      end

    end 

  end

  describe 'rate_for' do
    before(:each) do
      @call_rate = create(:call_rate, incoming_rate: 0.1, outgoing_rate: 0.2)
    end
    it 'return incoming_rate for incoming call' do
      call = Service::CallLog.new direction: 'incoming'
      expect(@call_rate.rate_for(call)).to eq 0.1
    end

    it 'return outgoing_rate for outgoin call' do
      call = Service::CallLog.new direction: 'outgoing'
      expect(@call_rate.rate_for(call)).to eq 0.2
    end
  end

  describe '.has_address?' do
    it 'return true for address that has call_rate.prefix' do
      call_rate = create(:call_rate, prefix: '012,098,015')

      expect(call_rate.has_address?('012123123')).to be true
      expect(call_rate.has_address?('098123123')).to be true
      expect(call_rate.has_address?('015123123')).to be true
    end

    it 'return false for address that does not start with call_rate.prefix' do
      call_rate = create(:call_rate, prefix: '012,098,015')

      expect(call_rate.has_address?('001212313')).to be false
      expect(call_rate.has_address?('980980980')).to be false
      expect(call_rate.has_address?('010150150')).to be false
    end
  end

  describe '.has_prefix?' do
    it 'return true for prefix that has call_rate.prefix' do
      call_rate = create(:call_rate, prefix: '012,098,015')

      expect(call_rate.has_prefix?('012,00,89')).to be true
      expect(call_rate.has_prefix?('00,012,01')).to be true
      expect(call_rate.has_prefix?('01,09,098')).to be true
    end

    it 'return false for prefix that does not start with call_rate.prefix' do
      call_rate = create(:call_rate, prefix: '012,098,015')

      expect(call_rate.has_prefix?('0122,0982,0150')).to be false
      expect(call_rate.has_prefix?('0983,093')).to be false
      expect(call_rate.has_prefix?('2012,0984,0159')).to be false
    end

  end

end
