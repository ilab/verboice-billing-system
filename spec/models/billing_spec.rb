require 'spec_helper'

describe Billing do
  before(:each) do
    @channel = Service::Channel.new(id: 9999)

    call_rate1 = create(:call_rate, prefix: '010,069',
      incoming_rate: 20, outgoing_rate: 40, round_rate: 30, channel_id: @channel.id)

    call_rate2 = create(:call_rate, prefix: '093',
      incoming_rate: 1, outgoing_rate: 2, round_rate: 20, channel_id: @channel.id)

    @call_rates = [call_rate1, call_rate2]

    @call_log1 = Service::CallLog.new(address: '010 212121', duration: 80, direction: 'outgoing', channel: @channel)
    @call_log2 = Service::CallLog.new(address: '095 212121', duration: 40, direction: 'incoming', channel: @channel)
    @call_log3 = Service::CallLog.new(address: '069 212121', duration: 40, direction: 'incoming', channel: @channel)

    @call_logs = [@call_log1, @call_log2, @call_log3]
  end

  describe '.process!' do
    context 'billing for all' do
      it 'process the logs and calculate total' do
        billing = Billing.new(@channel.id)
        billing.process!(@call_logs)

        expect(billing.calls.count).to eq 3
        expect(billing.has_error?).to be true
        expect(billing.total).to eq total(billing)
      end
    end

    context 'billing for error' do
      it 'process the logs and total for error call_logs only' do
        billing = Billing.new(@channel.id)
        billing.process!(@call_logs)
        billing.error_calls!
        
        expect(billing.calls.count).to eq 1
        expect(billing.has_error?).to be true
        expect(billing.total).to eq total(billing)
      end
    end

  end

  def total(billing)
    billing.calls.inject(0) { |sum, call| sum + call.cost }
  end
end
