require 'spec_helper'

describe Session do
  before(:each ) do
    @url  = 'http://localhost:3030/api/channels'
    @user = 'channa.ly@instedd.org'
    @pwd  = '123456'
  end

  # it 'set token with init_with' do
  #     token = '1asdbf1f1f1f1f1f3452663'
  #     email = 'admin@example.com'
  #     url  = 'http://localhost:3001'

  #     Session.init_with({token: token, email: email}, url)
  #     expect(Session.end_point).to eq url
  #     expect(Session.credential).to eq({token: token, email: email})
  # end

  describe '.login' do
    context 'with valid endpoint and credential' do
      it 'return successs result' do
        # stub_request(:post, url).with(request_options)   
        VCR.use_cassette 'models/login-success' do              
          response = Session.login(user_params[:url], user_params[:email], user_params[:password])
          expect(response[:success]).to eq true
          expect(response[:auth_token]).not_to be_empty
          expect(response[:email]).to eq user_params[:email]
          expect(Session.success?).to eq true
        end
        
      end
    end

    context 'with valid endpoint and invalid credential' do
      it 'return successs result' do
        password = 'password2'
        email = '1@example.com'
        url  = 'http://localhost:3000/api2'

        VCR.use_cassette 'models/login-invalid-credential' do  
         response = Session.login(url, email, password)
         expect(response[:success]).to be false
         expect(response[:message]).to eq 'Error with your login or password'
         expect(Session.success?).to be false
        end
      end
    end

    context 'with invalid endpoint' do
      it 'return error result' do
        password = 'passwords'
        email = '1@example.com'
        url  = 'http://localhost:3000/api'

        VCR.use_cassette 'models/login-invalid-endpoint' do
          response = Session.login(url, email, password)
          expect(response[:success]).to be false
          expect(response[:message]).to eq 'Page not found'
          expect(Session.success?).to be false
        end
      end
    end
  end
end
