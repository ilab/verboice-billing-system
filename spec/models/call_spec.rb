require 'spec_helper'

describe Call do
  describe '#cost' do
    let(:call_log) {
      Service::CallLog.new(address: '010 212121', duration: 80, direction: 'outgoing', channel: @channel)
    }

    let(:call_rate) {
      create(:call_rate, prefix: '010,069',
        incoming_rate: 20,
        outgoing_rate: 40,
        round_rate: 30)
    }

    context 'address has prefix registered' do
      it 'return cost object with given calculate price' do
        call = Call.new(call_log, call_rate)
        
        expect(call.cost).to eq 60
      end
    end

    context 'address has no prefix registered' do
      let(:call_log) {  }
      let(:call_rate) {  }

      it 'return cost object with error' do
        call = Call.new(call_log)
        
        expect(call.cost).to eq 0.0
        expect(call.has_error?).to be true
      end
    end
  end
end
