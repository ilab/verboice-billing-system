require 'spec_helper'

describe QuotaHistory do
  describe '.create_from quota' do
    context 'new due_date' do
      it 'save quota_history' do
        quota = create(:quota, from_date: '2014-08-01', to_date: '2014-08-31')
        history = QuotaHistory.create_from(quota)
        expect(QuotaHistory.count).to eq 1
        expect(history.due_date).to eq Date.new(2014, 9, 1)
      end
    end

    context 'old due date' do
      it 'should update history field' do
        quota = create(:quota, from_date: '2014-08-01', to_date: '2014-08-31',
          used: 40, total: 60)

        history = QuotaHistory.create_from(quota)
        expect(QuotaHistory.count).to eq 1

        expect(history.total).to eq 60
        expect(history.used).to eq 40
      end

    end


  end
end
