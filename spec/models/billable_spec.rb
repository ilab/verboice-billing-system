require 'spec_helper'

describe Billable do
  let(:channel) { Service::Channel.new(id: 9999) }
  
  let(:call_rate) { create(:call_rate, prefix: '010',
      incoming_rate: 20, outgoing_rate: 40, round_rate: 30, channel_id: channel.id) }
  let(:call_rates) { [call_rate] }
  
  let(:call_log1) { Service::CallLog.new(address: '010 212121', duration: 80, direction: 'outgoing', channel: channel) }
  let(:call_log2) { Service::CallLog.new(address: '095 212121', duration: 40, direction: 'incoming') }
  let(:call_log3) { Service::CallLog.new(address: '095 212121', duration: 40, direction: 'incoming') }
  let(:call_logs) { [call_log1, call_log2, call_log3] }

  let(:billable) { Billable.new(call_rates: call_rates) }

  describe '#for!' do
    context 'list all calls those billable' do
      it { expect(billable.for(call_logs).select { |call| !call.has_error? }.count ).to eq(1) }
    end

    context 'list all calls those are unbillable' do
      it { expect(billable.for(call_logs).select { |call| call.has_error? }.count ).to eq(2) }
    end
  end
end
