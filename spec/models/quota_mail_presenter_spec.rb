require 'spec_helper'

describe QuotaMailPresenter do
  describe '.by quota' do
    it 'send email when reaching a  warning level' do
      instance = create(:instance, name: 'verboice.com')
      quota = create(:quota, total: 100, used: 80,
                                         percentage_condition: 30.0,
                                         alert_email: 'a@domain.com,b@domain.com',
                                         instance: instance
                                         )
      expect { QuotaMailPresenter.by(quota) }.to change { ActionMailer::Base.deliveries.count }.by(1)
      ActionMailer::Base.deliveries

      mailer1 = ActionMailer::Base.deliveries.last
      expect(mailer1.to).to eq ['a@domain.com', 'b@domain.com']
      expect(mailer1.subject).to eq "Quota reached 30.0% on verboice.com"
    end
  end
end