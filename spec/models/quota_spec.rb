require 'spec_helper'

describe Quota do
  describe 'set_call_blocked' do
    context 'quota reached' do
      it 'set blocked to be true if block_if_reached is true' do
        quota = create(:quota, total: 100, used: 100, block_if_reached: true)
        expect(quota.blocked).to be true
      end

      it 'set blocked to be false if block_if_reached is false' do
        quota = create(:quota, total: 100, used: 100, block_if_reached: false)
        expect(quota.blocked).to be false
      end
    end

    context 'quota not reached' do
      it 'set blocked to be false' do
        quota = create(:quota, total: 100, used: 99, block_if_reached: true, blocked: true)
        expect(quota.blocked).to be false
      end
    end
  end
end
