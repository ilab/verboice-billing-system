# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :call_rate, class: CallRate do
    sequence(:account_id) {|n| n }
    sequence(:account_email) { |n| "account_email_#{n}@examle.com" }

    sequence(:channel_id) {|n| "channel_#{n}"}
    sequence(:channel_name) {|n| "channel_#{n}"}

    sequence(:prefix) {|n| "#{n}"}
    instance
    incoming_rate 1
    outgoing_rate 2
    round_rate 15
    note "MyText"
  end
end
