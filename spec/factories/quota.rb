# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :quota, class: Quota do

    total 1.5
    used 1.5
    instance

    from_date "2014-08-01"
    to_date   "2014-08-31"

    sequence(:channel_id) {|n| n}
    sequence(:channel_name) { |n| "channel_name_#{n}"}

    sequence(:account_id) {|n| n}
    sequence(:account_email) {|n| "email+#{n}@gmail.com"}
  end
end