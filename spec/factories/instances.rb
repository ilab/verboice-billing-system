# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :instance, class: Instance do
    sequence(:name) { |n| "name_#{n}" }
    sequence(:url) { |n| "http://example_#{n}.com" }
    sequence(:end_point) { |n| "http://example.com/api#{n}" }
  end
end
