module Features
  module SessionHelpers
    def sign_in
      VCR.use_cassette 'features/user_sign_in_valid' do
        FactoryGirl.create(:instance, name: 'http://localhost:3000/api2', url: 'http://localhost:3000', end_point: 'http://localhost:3000/api2')

        visit sign_in_path
        select 'http://localhost:3000/api2', :from => 'Endpoint'
        fill_in "Email",  with: user_params[:email]
        fill_in "Password", with: user_params[:password]
        click_button "Login"
      end
    end
  end
end