require 'spec_helper'

feature 'User sign in', :type => :feature do

  scenario 'with valid attribute' do
    VCR.use_cassette 'features/user_sign_in_valid' do
      create(:instance, name: 'http://localhost:3000/api2', end_point: 'http://localhost:3000/api2')

      visit sign_in_path
      select 'http://localhost:3000/api2', :from => 'Endpoint'
      fill_in "Email",  with: user_params[:email]
      fill_in "Password", with: user_params[:password]
      click_button "Login"

      expect(page).to have_content('Sign out')
      expect(page).to have_content(user_params[:email])
    end

  end

  scenario 'with empty attributes' do
    VCR.use_cassette 'features/user_sign_in_empty' do
      visit sign_in_path
      select '', :from => 'Endpoint'
      fill_in "Email",  with: ''
      fill_in "Password", with: ''
      click_button "Login"

      expect(page).to have_content("can't be blank")
      expect(page).to have_selector('.help-inline', count: 3)
    end
  end

  scenario 'with invalid endpoint' do
    VCR.use_cassette 'features/user_sign_in_empty' do
      create(:instance, name: 'http://localhost:3000', end_point: 'http://localhost:3000')

      visit sign_in_path
      select 'http://localhost:3000', :from => 'Endpoint'
      fill_in "Email",    with: user_params[:email]
      fill_in "Password", with: user_params[:password]
      click_button "Login"

      expect(page).to have_content("Failed to login")

    end
  end



end
