require 'spec_helper'

feature 'Destroy an instance', :type => :feature do
  scenario 'ignore when total instances less or equal 1' do
    sign_in

    visit instances_path

    expect {
      click_link("Delete", match: :first)

      expect(page).to have_content('Instance must be at least one')
    }.to change(Instance, :count).by(0)
  end

  scenario 'proceed when total instances greater than 1' do
    FactoryGirl.create(:instance)

    sign_in

    visit instances_path

    expect {
      click_link("Delete", match: :first)

      expect(page).to have_content('has been deleted')
    }.to change(Instance, :count).by(-1)
  end

end
