require 'spec_helper'

feature 'Create a new instance', :type => :feature do
  scenario 'with a valid attributes' do
    sign_in

    visit new_instance_path

    fill_in 'Name',       :with => 'foo'
    fill_in 'Url',      :with => 'http://example.com'
    fill_in 'End point',  :with => 'http://example.com/api2'
    click_button 'Save'

    expect(page).to have_content 'Instance has been created'
  end

  scenario 'with an invalid url' do
    sign_in

    visit new_instance_path

    fill_in 'Name',       :with => 'foo'
    fill_in 'Url',      :with => 'foo'
    fill_in 'End point',  :with => 'http://example.com/api2'
    click_button 'Save'

    expect(page).to have_content 'Failed to create instance'
  end

  scenario 'with an invalid end_point' do
    sign_in

    visit new_instance_path

    fill_in 'Name',       :with => 'foo'
    fill_in 'Url',      :with => 'http://example.com'
    fill_in 'End point',  :with => 'foo'
    click_button 'Save'

    expect(page).to have_content 'Failed to create instance'
  end
end
