namespace :quota do
  Rails.logger = Logger.new(STDOUT)

  desc 'Update billing data from verboice'
  task :update => :environment do
    Instance.all.each do |instance|
      from_date = Time.now.beginning_of_month
      to_date   = from_date.end_of_month
      format    = "%Y-%m-%d"
      update_instance_quota(instance, from_date.strftime(format), to_date.strftime(format))
    end
  end

  desc 'Reset quota'
  task :reset => :environment do
    Instance.all.each do |instance|
      reset_instance_quota(instance)
    end
  end
end

def auth_error
  lambda { |instance| Rails.logger.debug "Could not be authenticated to: #{instance.end_point}" }
end

def reset_instance_quota instance
  QuotaSync.with_credential instance, success: reset_quota, error: auth_error
end

def reset_quota
  lambda { |instance, data|

    Rails.logger.info("Reset quota")
    quotas = Quota.by_instance(instance).as_enabled

    quotas.each do |quota|
      QuotaHistory.create_from(quota)

      options = {
        enabled: quota.enabled,
        blocked: quota.blocked
      }

      quota.email_sent = nil
      quota.used = 0

      quota.from_date = Time.zone.now.beginning_of_month
      quota.to_date   = quota.from_date.end_of_month

      quota.save
      Service::ChannelQuota.update_quota quota, options
    end
  }
end

def update_instance_quota instance, from_date, to_date
  options = { success: update_quota,
              error: auth_error,
              data: { from_date: from_date, to_date: to_date }
            }
  QuotaSync.with_credential instance, options
end

def update_quota
  lambda { |instance, data|
    quotas = Quota.as_enabled.by_instance(instance).includes(:instance)

    return Rails.logger.debug("No quotas are enabled. make sure your have quotas to update") if quotas.count == 0
    channel_ids   = quotas.map(&:channel_id).join(',')

    call_logs  = Service::CallLog.filter(channel_ids, data[:from_date], data[:to_date])
    return Rails.logger.debug("No call_logs found for channels #{channel_ids} ") if call_logs.count == 0

    quotas.each do |quota|
      call_rates = CallRate.where(['channel_id = :channel_id', channel_id: quota.channel_id ])
      call_logs_by_channels = call_logs.select { |call_log| call_log.channel.id == quota.channel_id }

      # billing = Billing.new call_logs_by_channels, call_rates, quota.channel_id
      billing = Billing.new quota.channel_id
      billing.process! call_logs_by_channels

      used = billing.total
      quota.used = used
      quota.from_date = data[:from_date]
      quota.to_date   = data[:to_date]

      options = {
        enabled: quota.enabled,
        blocked: quota.blocked
      }

      quota = QuotaMailPresenter.by(quota)
      quota.save

      QuotaHistory.create_from(quota)

      Service::ChannelQuota.update_quota(quota, options)
      Rails.logger.info "Updated quota to: #{used}"
    end
  }
end
